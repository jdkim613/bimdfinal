// File: tmdb_api.js
var movie_cell_template = "<div class='col-sm-3 movie-cell'><div class='movie-poster'><img></div><div class='movie-title'></div></div>";
var search_term = "";
var search_base_url = "https://api.themoviedb.org/3/search/movie?";
var poster_base_url = "https://image.tmdb.org/t/p/w500"
var search_api_url;   // use this string to build your search from search_url and search_term

// use this function to concantenate (build) your search url
function getSearchUrl(queryString) {
  var url = "<BUILD YOUR API MOVIE SEARCH URL HERE!>";
  return url;
}

// use this function to concantenate (build) your poster url
function getPosterUrl(imageString) {
  var url = "<BUILD YOUR POSTER URL HERE!>";
  return url;
}

// Shorthand for $( document ).ready()
$(function() {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 1: key input processing
  // ========================================================
  $('input').on({
    // keydown - backspace or delete processing only
    keydown: function(e) {
      var code = e.which;
      console.log('KEYDOWN-key:' + code);
    },
    // keypress - everything else processing
    keypress: function(e) {
      var code = e.which;
      console.log('KEYPRESS-key:' + code);
    }
  });
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 2:  process search on omdb
  // ========================================================
  $('#get-movie-btn').on('click', api_search);

  // Do the actual search in this function
  function api_search(e) {
    // prepare search string url => search_api_url
    $.ajax({
      url: search_api_url,
      success: function(data) {
        render(data);
      },
      cache: false
    });
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 3: perform all movie grid rendering operations
  // ========================================================
  function render(data) {
    // get movie data from search results data

    // select the movie grid and dump the sample html (if any)

    // every group of 4 gets their own class=row

    // utilize the movie_cell_template to append, then add data from
    // movie data results (parsed JSON)
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
});
