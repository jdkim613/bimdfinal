// File: tmdb_api.js
var movie_cell_template = "<div class='col-sm-3 movie-cell'><div class='movie-poster'><img></div><div class='movie-title'></div></div>";
var search_term = "";
var search_base_url = "https://api.themoviedb.org/3/search/movie?api_key=0a04846e96d5318f25099bd20f5cb09c&language=en-US&query=";
var poster_base_url = "https://image.tmdb.org/t/p/w500"
var search_end_url = "&page=1&include_adult=false";
   // use this string to build your search from search_url and search_term

// use this function to concantenate (build) your search url
function getSearchUrl(queryString) {
  var url = search_base_url + search_term + search_end_url;
  return url;
}

// use this function to concantenate (build) your poster url
function getPosterUrl(imageString) {
  var url = poster_base_url + poster_path;
  return url;
}


// Shorthand for $( document ).ready()
$(function() {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 1: key input processing
  // ========================================================
  $('input').on({
    // keydown - backspace or delete processing only
    keydown: function(e) {
      var code = e.which;
      console.log('KEYDOWN-key:' + code);
      var search_term = $('input').val();
      // var search_api_url = search_base_url + search_term + search_end_url;

        if (code === 13) {
          console.log(search_term);
        };
        // return search_api_url;
        return search_term;
    },
    // keypress - everything else processing
    keypress: function(e) {
      var code = e.which;
      console.log('KEYPRESS-key:' + code);
    }
  });

  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 2:  process search on omdb
  // ========================================================
  $('#get-movie-btn').on('click', api_search);

  // Do the actual search in this function
  function api_search(e) {
    var str = $("input").val();
    var search_api_url = "https://api.themoviedb.org/3/search/movie?api_key=0a04846e96d5318f25099bd20f5cb09c&language=en-US&query=" + str+ "&page=1&include_adult=false";

    // prepare search string url => search_api_url
    $.ajax({
      url: search_api_url, //"https://api.themoviedb.org/3/search/movie?api_key=0a04846e96d5318f25099bd20f5cb09c&language=en-US&query=star&page=1&include_adult=false",
      success: function(data) {
        render(data);

          var title = [];
          title.push(data['results'][0]['title']);
          title.push(data['results'][1]['title']);
          title.push(data['results'][2]['title']);
          title.push(data['results'][3]['title']);
          title.push(data['results'][4]['title']);
          title.push(data['results'][5]['title']);
          title.push(data['results'][6]['title']);
          title.push(data['results'][7]['title']);

          var poster_path = [];
          poster_path.push(data['results'][0]['poster_path']);
          poster_path.push(data['results'][1]['poster_path']);
          poster_path.push(data['results'][2]['poster_path']);
          poster_path.push(data['results'][3]['poster_path']);
          poster_path.push(data['results'][4]['poster_path']);
          poster_path.push(data['results'][5]['poster_path']);
          poster_path.push(data['results'][6]['poster_path']);
          poster_path.push(data['results'][7]['poster_path']);

          // for (var i = 0; i < john.length; i++)
          $('button').on('click', function(e) {
            $('#movie-grid div').remove()
          });

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[0] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[0] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[1] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[1] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[2] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[2] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[3] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[3] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[4] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[4] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[5] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[5] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[6] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[6] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');

          var str = "<div class='col-sm3'><li><h1 id='BigFont'> Title : " + title[7] + "</h1></li><br><center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path[7] + "''></center></div>";
          $('#movie-grid').append(str);
          $('ul li:last').attr('class', 'list-group-item');



      },
      cache: false

    });
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 3: perform all movie grid rendering operations
  // ========================================================
  function render(data) {
    // get movie data from search results data

    // select the movie grid and dump the sample html (if any)

    // every group of 4 gets their own class=row

    // utilize the movie_cell_template to append, then add data from
    // movie data results (parsed JSON)
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
});
