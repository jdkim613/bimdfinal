// File: tmdb_api.js
var movie_cell_template = "<div class='col-sm-3 movie-cell'><div class='movie-poster'><img></div><div class='movie-title'></div></div>";
var search_term ="";
var search_base_url = "https://api.themoviedb.org/3/search/movie?api_key=0a04846e96d5318f25099bd20f5cb09c&language=en-US&query=";
var poster_base_url = "https://image.tmdb.org/t/p/w500";

var search_end_url = "&page=1&include_adult=false";
var search_api_url = search_base_url;


console.log(search_api_url);
 // use this string to build your search from search_url and search_term



// use this function to concantenate (build) your search url
function getSearchUrl(queryString) {
  var url = search_term;
  return url;
}

// use this function to concantenate (build) your poster url
function getPosterUrl(imageString) {
  var url = poster_base_url + poster_path;
  return url;
}

// Shorthand for $( document ).ready()
$(function() {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 1: key input processing
  // ========================================================
  $('input').on({
    // keydown - backspace or delete processing only
    keydown: function(e) {
      var code = e.which;
      console.log('KEYDOWN-key:' + code);
      var search_term = $('input').val();


       if (code === 13) {
         $('#movie-grid').append('<li>' + search_term + '</li>');
        console.log(search_term);
         return search_term;

       }

    },
    // keypress - everything else processing
    keypress: function(e) {
      var code = e.which;
      console.log('KEYPRESS-key:' + code);
            var string = $('input').val();
    }
  });

  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 2:  process search on omdb
  // ========================================================
  $('#get-movie-btn').on('click', api_search);

  // Do the actual search in this function
  function api_search(e) {
    // prepare search string url => search_api_url
    $.ajax({
      url: search_api_url,
      dataType: "jsonp",
      success: function(data) {
        render(data);

        var title = data['results'][0]['title'];
        var poster_path = data['results'][0]['poster_path']

        var str = "<li><h1> Title : " + title + "</h1></li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');


        var str = "<center><img src=" + "'https://image.tmdb.org/t/p/w500"+ poster_path + "''></center>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
      },
      cache: false
    });
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 3: perform all movie grid rendering operations
  // ========================================================
  function render(data) {
    // get movie data from search results data
    var str = title + poster_path;
    $('#movie-grid').append(str);

    // select the movie grid and dump the sample html (if any)

    // every group of 4 gets their own class=row

    // utilize the movie_cell_template to append, then add data from
    // movie data results (parsed JSON)
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
});
